const electron = require('electron');
const url = require('url');
const path = require('path');

const {app, BrowserWindow, Menu, ipcMain} = electron;

let mainWindow;
let addWindow;

//listen for app to be ready
app.on('ready', function(){
  //create new window
  mainWindow = new BrowserWindow({
    width: 1920,
    height: 1080
  });
  // load html into window
  mainWindow.loadURL(url.format({
      pathname: path.join(__dirname, 'mainWindow.html'),
      protocol: 'file:',
      slashes: true
  }));
  mainWindow.setFullScreen(true);
  // quit app when closed
  mainWindow.on('closed', function(){
      app.quit();
  });
});
